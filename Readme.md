Enterprise application builded with WPF, MVVM and Entity Framework Code First.

Created a database with Entity Framework Code First.

Set up WPF app with MVVM design pattern.

Communication between ViewModels with Prism`s EventAggregator.

Validation of user input with Data Annotations & INotifyDataErrorInfo.

Used Autofac for Depenency Injection.

Used Mahapps.Metro for styling.

Code from Pluralsite course Building an Enterprise App with WPF, MVVM and Entity Framework Code First by Thomas Claudius Huber.